from django.apps import AppConfig


class TbotMessagesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tbot_messages'
    verbose_name = 'сообщения'
    verbose_name_plural = 'сообщения'
